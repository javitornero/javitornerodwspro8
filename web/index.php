<?php // web/index.php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once __DIR__ . '/../app/utils/utils_control.php';
inicializaModelo(); // Inicializa el modelo con valor mysql    
cambiaModelo(); // Escucha cuando se elije un modelo distinto y lo guarda en variable sesión

require_once __DIR__ . '/../app/Config.php'; // carga del modelo y los controladores
require_once __DIR__ . '/../app/ModeloFichero.php';
require_once __DIR__ . '/../app/ModeloMysql.php';
require_once __DIR__ . '/../app/Controller.php';
require_once __DIR__ . '/../app/ControlLocalizacion.php';
require_once __DIR__ . '/../app/ControlUsuario.php';


$map = array( // enrutamiento
	'inicio' => array('controller' =>'Controller', 'action' =>'inicio'),
	'createLoc' => array('controller' =>'ControlLocalizacion', 'action' =>'createLoc'),
	'readLoc' => array('controller' =>'ControlLocalizacion', 'action' =>'readLoc'),
	'updateLoc' => array('controller' =>'ControlLocalizacion', 'action' =>'updateLoc'),
	'delLoc' => array('controller' =>'ControlLocalizacion', 'action' =>'delLoc'),
	'createU' => array('controller' =>'ControlUsuario', 'action' =>'createU'),
	'readU' => array('controller' =>'ControlUsuario', 'action' =>'readU'),
	'updateU' => array('controller' =>'ControlUsuario', 'action' =>'updateU'),
	'delU' => array('controller' =>'ControlUsuario', 'action' =>'delU'),
	);

if (isset($_GET['ctl'])) { // Parseo de la ruta
	if (isset($map[$_GET['ctl']])) {
		$ruta = $_GET['ctl'];
	} else {
		header('Status: 404 Not Found'); //Si la opción seleccionada no existe en el array de mapeo, mostramos pantalla de error
		echo '<html><body><h1>Error 404: No existe la ruta <i>' .
		$_GET['ctl'] .'</p></body></html>';
		exit;
	}
} else {
	$ruta = 'inicio'; //Si no se ha seleccionado nada mostraremos pantalla de inicio
}

$controlador = $map[$ruta]; //Cargamos el asociado a la acción seleccionada por el usuario 

if (method_exists($controlador['controller'],$controlador['action'])) { // Ejecución del controlador asociado a la ruta
	call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {
	//Si no existe controlador asociado a la acción, mostramos pantalla de error
	header('Status: 404 Not Found');
	echo '<html><body><h1>Error 404: El controlador <i>' .
	$controlador['controller'] .'->' .	$controlador['action'] .'</i> no existe</h1></body></html>';
}
