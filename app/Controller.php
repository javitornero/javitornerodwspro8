<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Controller {

	function instalar(){
		$modelo=obtenerModelo();
		$respuesta= $modelo->instalar(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
		return $respuesta;
	}

	function validaAdministrador($nom, $password){
		$administrador=new Administrador("", $nom, $password, "");
		if ($_SESSION["modelo"]=="mysql") {
			$modelo=obtenerModelo();
			$administrador= $modelo->traeAdministrador($administrador);
		}
		if ($administrador->getNom()) {
			$_SESSION["nom"] = $administrador->getNom();
			$loginRes= "Bienvenido ".$_SESSION['nom'];
			$params= array('loginRes' => $loginRes,'fecha' => date('d-m-y'),);
			require __DIR__ . '/templates/inicio.php';
		}else{
			$loginRes= "Datos incorrectos. Revise los campos de entrada.";
			$params= array('loginRes' => $loginRes,);
			require __DIR__ . '/templates/login.php';
		}
	}

	public function inicio() {
		$_SESSION['testConexion']=Controller::conectar(); // Prueba de conexion
		if ($_SESSION['testConexion']!='OK') {
			if (isset($_REQUEST['sbInstalar'])) {
				$loginRes= Controller::instalar();
				$params= array('loginRes' => $loginRes);
				require __DIR__ . '/templates/login.php';
			}else{
				$mensaje= "No existe la base de datos ".Config::$bdnombre.". Para desplegarla correctamente debe disponer de un usuario llamado alumno con contraseña alumno en Mysql y hacer click en el siguiente botón.";
				$params= array('mensaje' => $mensaje);
				require __DIR__ . '/templates/install.php';
			}
		}else{
			if($_SESSION=='mysql'){ Controller::desconectar(); } // Prueba de conexion
			if (isset($_REQUEST['nom']) && isset($_REQUEST['password'])) { // A validarlo
				$nom = recoge("nom");
				$password = recoge("password");
				$controller= new Controller();
				$controller->validaAdministrador($nom, $password);
			}else if (isset($_REQUEST['sbCierraSesion'])) { // Si cierras sesión a login
				unset($_SESSION["nom"]);
				unset($_SESSION["modelo"]);
				unset($_SESSION["colInfo"]);
				unset($_SESSION["testConexion"]);
				session_destroy();
				$loginRes= "Ha cerrado la sesión";
				$params= array('loginRes' => $loginRes,);
				require __DIR__ . '/templates/login.php';
			}else if (isset($_REQUEST['sbModelo'])) { // Si se cambia de modelo a inicio
				$loginRes= 'Has cambiado al modelo '.$_SESSION['modelo'].".";
				$params= array('loginRes' => $loginRes,'fecha' => date('d-m-y'),);
				require __DIR__ . '/templates/inicio.php';
			}else if (isset($_SESSION['nom'])) { // Si estás validado a inicio
				$loginRes="";
				$params= array('loginRes' => $loginRes,'fecha' => date('d-m-y'),);
				require __DIR__ . '/templates/inicio.php';			
			}else{ // Cualquier otra eventualidad a login
				$loginRes="Pantalla de login";
				$params= array('loginRes' => $loginRes,);
				require __DIR__ . '/templates/login.php';
			}
		}
	}

	function conectar(){
		if ($_SESSION["modelo"]=="fichero") {
			$respuesta= "OK"; // Devuelvo OK porque los métodos de instalación, conexión y desconexión los veo necesarios solo para el modelo Mysql
		}else if($_SESSION["modelo"]=="mysql"){
			$modelo=obtenerModelo();
			$respuesta= $modelo->conectar();
		}
		return $respuesta;
	}

	function desconectar(){
		$modelo=obtenerModelo();
		return $modelo->desconectar();
	}
}

?>
