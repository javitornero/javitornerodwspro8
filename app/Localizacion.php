<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
/**
* 
*/
class Localizacion {
	//Propiedades
	private $id;
	private $nom;
	private $countU;
	//Constructor
	public function __construct($id, $nom, $countU) {
		$this->id = $id;
		$this->nom = $nom;
		$this->countU = $countU;
	}
	//Metodos
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getNom() {
		return $this->nom;
	}
	public function setNom($nom) {
		return $this->nom = $nom;
	}
	public function getCountU() {
		return $this->countU;
	}
	public function setCountU($countU) {
		return $this->countU = $countU;
	}
}
?>