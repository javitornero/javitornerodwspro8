<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
	require_once __DIR__ . "/IModelo.php";
	require_once __DIR__ . "/Localizacion.php";
	require_once __DIR__ . "/Usuario.php";

/**
* 
*/
class ModeloFichero implements IModelo{

	// Función que devuelve el siguiente id. Siguiente id = id_máximo +1
	function newId($fichero){
		$maxId=0;
		if (is_file('../app/txt/'.$fichero.".txt")) {
			$lineas = file('../app/txt/'.$fichero.".txt");
			foreach ($lineas as $linea) {
				$arrayLinea=explode(";", $linea);
				if($arrayLinea[0]>$maxId){
					$maxId=$arrayLinea[0]; // Guardamos el valor máximo
				}
			}
		}
		return $maxId+1; // Devolvemos valor máximo +1
	}

	//**********************************************************************
	//Create..

	// Función que recibe un objeto de la clase Localizacion y se vale de sus metodos get para obtener los datos y escribirlos en el fichero que corresponde
	function createLocalizacion($localizacion) {
		$f = fopen("../app/txt/localizaciones.txt", "a");
		$linea = $localizacion->getId().";".$localizacion->getNom()."\r\n";
		fwrite($f, $linea);
		fclose($f);
		return true;
	}

	// Función que recibe un objeto de la clase Usuario y se vale de sus metodos get para obtener os datos y escribirlos en el fichero que corresponde
	function createUsuario($usuario) {
		$f = fopen("../app/txt/usuarios.txt", "a");
		$linea = $usuario->getId().";".$usuario->getNom().";".$usuario->getLocalizacion()->getId()."\r\n";
		fwrite($f, $linea);
		fclose($f);
		return true;
	}

	//**********************************************************************
	//Read..

	function countUsuariosByLoc($localizacion){
		$usuariosCount=0;
		$usuarios=ModeloFichero::readUsuarios();
		if (isset($usuarios)) {
			foreach ($usuarios as $usuario) {
				if ($localizacion->getId()==$usuario->getLocalizacion()->getId()) {
					$usuariosCount++;
				}
			}
		}
		return $usuariosCount;
	}

	// Devuelve array con todos los objetos de la clase Localizacion
	function readLocalizaciones(){
		if (is_file('../app/txt/localizaciones.txt')) {
			$localizaciones=array();
			$i=0;
			$lineas = file('../app/txt/localizaciones.txt');
			foreach ($lineas as $linea) {
				$arrayLinea=explode(";", $linea);
				$localizacion=new Localizacion($arrayLinea[0], $arrayLinea[1], "");
				$countU=ModeloFichero::countUsuariosByLoc($localizacion);
				$localizaciones[$i]=new Localizacion($arrayLinea[0], $arrayLinea[1], $countU);
				$i++;
			}
			return $localizaciones;
		}
	}

	// Devuelve array con todos los objetos de la clase Usuario
	function readUsuarios(){
		if (is_file('../app/txt/usuarios.txt')) {
			$arrayObj=array();
			$i=0;
			$lineas = file('../app/txt/usuarios.txt');
			foreach ($lineas as $linea) {
				$arrayLinea=explode(";", $linea);
				$localizacion=new Localizacion($arrayLinea[2],"n/a","n/a");
				$lineasLoc = file('../app/txt/localizaciones.txt');
				foreach ($lineasLoc as $lineaLoc) {
					$arrayLineaLoc=explode(";", $lineaLoc);
					if (intval($arrayLineaLoc[0])==intval($arrayLinea[2])) {
						$localizacion=new Localizacion($arrayLineaLoc[0], $arrayLineaLoc[1],"");
					}
				}
				$arrayObj[$i]=new Usuario($arrayLinea[0], $arrayLinea[1], $localizacion);
				$i++;
			}
			return $arrayObj;
		}
	}

	//**********************************************************************
	//Update..

	function updateLocalizacion($localizacion){
		if (is_file('../app/txt/localizaciones.txt')) {
			$i=0;
			$lineas = file('../app/txt/localizaciones.txt');
			foreach ($lineas as $linea) { // obtengo array de objetos
				$arrayLinea=explode(";", $linea);
				$arrayObjsLoc[$i]=new Localizacion($arrayLinea[0], $arrayLinea[1]);
				$i++;
			}
			for($i=0;$i<count($arrayObjsLoc);$i++){ // comparo y actualizo
				if ($arrayObjsLoc[$i]->getId()==$localizacion->getId()) {
					$arrayObjsLoc[$i]->setNom($localizacion->getNom()."\r\n");
				}
				$newArrayObjsLoc[]=$arrayObjsLoc[$i]; // genero nueva array
			}
			unlink("../app/txt/localizaciones.txt"); // Borro el texto	
		    $fich = fopen("../app/txt/localizaciones.txt", 'c'); // Abrir el fichero para sólo escritura. Si el fichero no existe, se crea.
		    for($i=0;$i<count($newArrayObjsLoc);$i++){ // escritura del nuevo array con los cambios hechos
		    	$linea=$newArrayObjsLoc[$i]->getId().";".$newArrayObjsLoc[$i]->getNom();
		    	fwrite($fich, $linea);
		    }
		    fclose($fich);
		    return true;
		}
	}

	function updateUsuario($usuario){
		if (is_file('../app/txt/usuarios.txt')) {
			$i=0;
			$lineas = file('../app/txt/usuarios.txt');
			foreach ($lineas as $linea) { // obtengo array de objetos
				$arrayLinea=explode(";", $linea);
				$arrayObjsU[$i]=new Usuario($arrayLinea[0], $arrayLinea[1], new Localizacion($arrayLinea[2], ""));
				$i++;
			}
			for($i=0;$i<count($arrayObjsU);$i++){ // comparo y actualizo
				if ($arrayObjsU[$i]->getId()==$usuario->getId()) {
					$arrayObjsU[$i]->setNom($usuario->getNom());
					$arrayObjsU[$i]->getLocalizacion()->setId($usuario->getLocalizacion()->getId()."\r\n");
				}
				$newArrayObjsU[]=$arrayObjsU[$i]; // genero nueva array
			}
			unlink("../app/txt/usuarios.txt"); // Borro el texto	
		    $fich = fopen("../app/txt/usuarios.txt", 'c'); // Abrir el fichero para sólo escritura. Si el fichero no existe, se crea.
		    for($i=0;$i<count($newArrayObjsU);$i++){ // escritura del nuevo array con los cambios hechos
	    		$linea=$newArrayObjsU[$i]->getId().";".$newArrayObjsU[$i]->getNom().";".$newArrayObjsU[$i]->getLocalizacion()->getId();
		    	fwrite($fich, $linea);
		    }
		    fclose($fich);
		    return true;
		}
	}

	//**********************************************************************
	//Delete..


	function deleteLocalizacion($localizacion){
		if (is_file('../app/txt/localizaciones.txt')) {
			if (is_file('../app/txt/usuarios.txt')) {
				$i=0;
				$lineas = file('../app/txt/usuarios.txt');
				foreach ($lineas as $linea) { // genero array de objetos
					$arrayLinea=explode(";", $linea);
					$arrayObjsU[$i]=new Usuario($arrayLinea[0], $arrayLinea[1], new Localizacion($arrayLinea[2],""));
					$i++;
				}
				unlink("../app/txt/usuarios.txt"); // Borro el texto	
				$fich = fopen("../app/txt/usuarios.txt", 'c'); // Abrir el fichero para sólo escritura. Si el fichero no existe, se crea.
				for($i=0;$i<count($arrayObjsU);$i++){ // Borrado en cascada de los usuarios de esa localizacion
					if ($arrayObjsU[$i]->getLocalizacion()->getId()!=intval($localizacion->getId())) {
						$newArrayObjsU[]=$arrayObjsU[$i]; // elimino no quedandome con la coincidencia en el nuevo array
					}
				}
				if (isset($newArrayObjsU)) { // corrige error de fichero vacio		   
					for($i=0;$i<count($newArrayObjsU);$i++){
						$linea=$newArrayObjsU[$i]->getId().";".$newArrayObjsU[$i]->getNom().";".$newArrayObjsU[$i]->getLocalizacion()->getId();
						fwrite($fich, $linea);
					}
				} else{
					fwrite($fich, "");
				}
				fclose($fich);
			}
			$i=0;
			$lineas = file('../app/txt/localizaciones.txt');
			foreach ($lineas as $linea) { // genero array de objetos
				$arrayLinea=explode(";", $linea);
				$arrayObjsLoc[$i]=new Localizacion($arrayLinea[0], $arrayLinea[1]);
				$i++;
			}
			unlink("../app/txt/localizaciones.txt"); // Borro el texto	
			$fich = fopen("../app/txt/localizaciones.txt", 'c'); // Abrir el fichero para sólo escritura. Si el fichero no existe, se crea.	
			for($i=0;$i<count($arrayObjsLoc);$i++){ // elimino no quedandome con la coincidencia en el nuevo array
				if ($arrayObjsLoc[$i]->getId()!=$localizacion->getId()) {
					$newArrayObjsLoc[]=$arrayObjsLoc[$i];
				}
			}
			if (isset($newArrayObjsLoc)) { // corrige error de fichero vacio
				for($i=0;$i<count($newArrayObjsLoc);$i++){
					$linea=$newArrayObjsLoc[$i]->getId().";".$newArrayObjsLoc[$i]->getNom();
					fwrite($fich, $linea);
				}
			} else {
				fwrite($fich, "");
			}
			fclose($fich);
			return true;
		}
	}

	function deleteUsuario($usuario){
		if (is_file('../app/txt/usuarios.txt')) {
			$i=0;
			$lineas = file('../app/txt/usuarios.txt');
			foreach ($lineas as $linea) { // genero array de objetos
				$arrayLinea=explode(";", $linea);
				$arrayObjsU[$i]=new Usuario($arrayLinea[0], $arrayLinea[1], new Localizacion($arrayLinea[2],""));
				$i++;
			}
			unlink("../app/txt/usuarios.txt"); // Borro el texto	
			$fich = fopen("../app/txt/usuarios.txt", 'c'); // Abrir el fichero para sólo escritura. Si el fichero no existe, se crea.
			for($i=0;$i<count($arrayObjsU);$i++){
				if ($arrayObjsU[$i]->getId()!=$usuario->getId()) {
					$newArrayObjsU[]=$arrayObjsU[$i]; // elimino no quedandome con la coincidencia en el nuevo array
				}
			}
			if (isset($newArrayObjsU)) { // corrige error de fichero vacio
			    for($i=0;$i<count($newArrayObjsU);$i++){
			    	$linea=$newArrayObjsU[$i]->getId().";".$newArrayObjsU[$i]->getNom().";".$newArrayObjsU[$i]->getLocalizacion()->getId();
			    	fwrite($fich, $linea);
			    }
			} else{
				fwrite($fich, "");
			}
			fclose($fich);
			return true;		
		}	
	}
}

?>