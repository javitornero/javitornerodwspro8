<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
/**
* 
*/
class Administrador {
	//Propiedades
	private $id;
	private $nom;
	private $password;
	private $tipo;
	//Constructor
	public function __construct($id, $nom, $password, $tipo) {
		$this->id = $id;
		$this->nom = $nom;
		$this->password = $password;
		$this->tipo = $tipo;
	}
	//Metodos
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getNom() {
		return $this->nom;
	}
	public function setNom($nom) {
		return $this->nom = $nom;
	}
	public function getPassword() {
		return $this->password;
	}
	public function setPassword($password) {
		return $this->password = $password;
	}
	public function getTipo() {
		return $this->tipo;
	}
	public function setTipo($Tipo) {
		return $this->tipo = $tipo;
	}
}
?>