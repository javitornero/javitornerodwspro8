<?php // templates/usuarioRead.php
ob_start()
?>

<div class="divReadRes" id="divReadU">
    <h1>Usuarios</h1>
    <div class="panelScroll">
    <?php
        echo "<table>";
        echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Localización</td><td class='colTabla'></td><td class='colTabla'></td></tr>";
        if (is_array($params['usuarios'])) { // Si es un array nos llegan todas los usuarios y las pintamos
            $row=count($params['usuarios']);
            for($i=0;$i<$row;$i++){
                echo "<tr class='filaTabla'>";
                echo "<td class='colTabla'>".$params['usuarios'][$i]->getId()."</td>";
                echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$params['usuarios'][$i]->getNom()."</td>";
                echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$params['usuarios'][$i]->getLocalizacion()->getNom()."</td>";
                echo "<td class='colTabla'><a class='a--update' href='index.php?ctl=updateU&id=".$params['usuarios'][$i]->getId()."&nom=".$params['usuarios'][$i]->getNom()."&id_loc=".$params['usuarios'][$i]->getLocalizacion()->getId()."'>Actualizar</a></td>";
                echo "<td class='colTabla'><a class='a--delete' href='index.php?ctl=delU&id=".$params['usuarios'][$i]->getId()."&nom=".$params['usuarios'][$i]->getNom()."'>Elimninar</a></td>";
                echo "</tr>";
            }
        }else{ // Si no es un array es que devuelve un mensaje de error
            echo $params['usuarios'];
        }
        echo "</table>";?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
