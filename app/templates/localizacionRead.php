<?php // templates/localizacionRead.php
ob_start()
?>

<div class="divReadRes" id="divReadLoc">
    <h1>Localizaciones</h1>
    <div class="panelScroll">
    <?php
        echo "<table>";
        echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Usuarios</td><td class='colTabla'></td><td class='colTabla'></td></tr>";
        if (is_array($params['localizaciones'])) { // Si es un array nos llegan todas las localizaciones y las pintamos
            $row=count($params['localizaciones']);
            for($i=0;$i<$row;$i++){
                echo "<tr class='filaTabla'>";
                echo "<td class='colTabla'>".$params['localizaciones'][$i]->getId()."</td>";
                echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$params['localizaciones'][$i]->getNom()."</td>";
                echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$params['localizaciones'][$i]->getCountU()."</td>";
                echo "<td class='colTabla'><a class='a--update' href=index.php?ctl=updateLoc&id=".$params['localizaciones'][$i]->getId()."&nom=".$params['localizaciones'][$i]->getNom().">Actualizar</a></td>";
                echo "<td class='colTabla'><a class='a--delete' href=index.php?ctl=delLoc&id=".$params['localizaciones'][$i]->getId().">Eliminar</a></td>";
                echo "</tr>";
            }
        }else{ // Si no es un array es que devuelve un mensaje de error
            echo $params['localizaciones'];
        }
        echo "</table>";?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
