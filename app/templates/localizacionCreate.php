<?php // templates/localicacionCreate.php
ob_start()
?>

<div class="contenedorForm">
    <span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
    <div class="divCRUD" id="divCreateLoc">
        <h1>Nueva localización</h1>
        <form method="POST" action="index.php?ctl=createLoc" >
            <input type="hidden" name="id"  value="<?php echo $params['id'] ?>"  />
            <table>
                <tr>
                    <td class="tdCRUD">Nombre nueva localización: </td>
                    <td class="tdCRUD"><input class="textCRUD" type="text" name="nom" value="<?php echo $params['nom'] ?>" required /></td>
                </tr>
                <tr>
                    <td class="tdCRUD"><input type="submit" name="sbCreateLoc" value="Crear"></td>
                    <td class="tdCRUD"><input type="reset" name="Borrar"></td>
                </tr>
            </table>        
        </form>
        <?php if(isset($params['mensaje'])){ echo $params['mensaje']; }?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
