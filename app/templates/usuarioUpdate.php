<?php // templates/usuarioUpdate.php
ob_start()
?>

<div class="contenedorForm">
    <span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
    <div class="divCRUD" id="divUpDateU">
        <h1>Actualizar usuario</h1>
        <form method="POST" action="index.php?ctl=updateU" >
            <input class="textCRUD" type="hidden" name="id" value="<?php echo $params['id'] ?>" />
            <table>
                <tr>
                    <td class="tdCRUD">Nombre: </td>
                    <td class="tdCRUD"><input class="textCRUD" type="text" name="nom" value="<?php echo $params['nom'] ?>" required /></td>
                </tr>
                <tr>
                    <td class="tdCRUD">Localización: </td>
                    <td class="tdCRUD">
                        <select name="id_loc" required >
                        <?php 
                            if ($params['localizaciones']) {
                                echo "<option selected disabled>Elige opción</option>";
                            } else{
                                echo "<option selected disabled>No hay localizaciones creadas.</option>";   
                            }
                            for($i=0;$i<count($params['localizaciones']);$i++){
                                if (isset($params['id_loc'])) {
                                    if ($params['id_loc']==$params['localizaciones'][$i]->getId()) {
                                        echo "<option selected value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                    }else{
                                        echo "<option value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                    }
                                }else{
                                    echo "<option value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                }
                            }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdCRUD"><input type="submit" name="sbUpdateU" value="Actualizar"></td>
                    <td class="tdCRUD"><input type="reset" name="Borrar"></td>
                </tr>
            </table>
        </form>
        <?php if(isset($params['mensaje'])){ echo $params['mensaje']; }?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
