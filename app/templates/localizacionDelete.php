<?php // templates/localizacionDelete.php
ob_start()
?>

<div class="contenedorForm">
    <span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
        <div class="divCRUD" id="divDelLoc">
        <h1>Eliminar localización</h1>
        <form method="POST" action="index.php?ctl=delLoc" >
            <table>
                <tr>
                    <td class="tdCRUD">Seleccione localización a eliminar: </td>
                    <td class="tdCRUD">
                        <select name="id" required >
                        <?php 
                            if ($params['localizaciones']) {
                                echo "<option selected disabled>Elige opción</option>";
                            } else{
                                echo "<option selected disabled>No hay localizaciones creadas.</option>";   
                            }
                            for($i=0;$i<count($params['localizaciones']);$i++){
                                if (isset($params['id'])) {
                                    if ($params['id']==$params['localizaciones'][$i]->getId()) {
                                        echo "<option selected value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                    }else{
                                        echo "<option value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                    }
                                }else{
                                    echo "<option value=".$params['localizaciones'][$i]->getId().">id: ".$params['localizaciones'][$i]->getId()." - ".$params['localizaciones'][$i]->getNom()."</option>";
                                }
                            }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdCRUD"><input type="submit" name="sbDeleteLoc" value="Eliminar"></td>
                    <td class="tdCRUD"><input type="reset" name ="Borrar"></td>
                </tr>
            </table>
        </form>
        <?php if(isset($params['mensaje'])){ echo $params['mensaje']; }?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
