<?php // templates/usuarioDelete.php
ob_start()
?>

<div class="contenedorForm">
    <span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
        <div class="divCRUD" id="divDelU">
        <h1>Eliminar usuario</h1>
        <form method="POST" action="index.php?ctl=delU" >
            <table>
                <tr>
                    <td class="tdCRUD">Seleccione usuario a eliminar: </td>
                    <td class="tdCRUD">
                        <select name="id" required >
                        <?php 
                            if ($params['usuarios']) {
                                echo "<option selected disabled>Elige opción</option>";
                            } else{
                                echo "<option selected disabled>No hay usuarios creados.</option>";   
                            }
                            for($i=0;$i<count($params['usuarios']);$i++){
                                if (isset($params['id'])) {
                                    if ($params['id']==$params['usuarios'][$i]->getId()) {
                                        echo "<option selected value=".$params['usuarios'][$i]->getId().">id: ".$params['usuarios'][$i]->getId()." - ".$params['usuarios'][$i]->getNom()."</option>";
                                    }else{
                                        echo "<option value=".$params['usuarios'][$i]->getId().">id: ".$params['usuarios'][$i]->getId()." - ".$params['usuarios'][$i]->getNom()."</option>";
                                    }
                                }else{
                                    echo "<option value=".$params['usuarios'][$i]->getId().">id: ".$params['usuarios'][$i]->getId()." - ".$params['usuarios'][$i]->getNom()."</option>";
                                }
                            }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdCRUD"><input type="submit" name="sbDeleteU" value="Eliminar"></td>
                    <td class="tdCRUD"><input type="reset" name ="Borrar"></td>
                </tr>
            </table>
        </form>
        <?php if(isset($params['mensaje'])){ echo $params['mensaje']; }?>
    </div>
</div>

<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>
