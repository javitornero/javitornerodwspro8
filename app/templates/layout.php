<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans+Mono" rel="stylesheet">
	<title><?php echo Config::$titulo; ?></title>
</head>
<body>
	<div class="divCabecera">
		<a class="aLogo" href="index.php">
			<div class="divLogo">
				Gestión de usuarios
			</div>
		</a>
		<div class="triangulo"></div>
		<nav class="menu">
		<?php
		if (isset($_SESSION['nom'])) { ?>
		    <ul class="menu--barra-principal">
		        <li class="menu--barra-principal--entrada">
		            <a href="#">Localizaciones</a>
		            <ul class="submenu">
		                <li id="createLoc" class="submenu--entrada"><a href="index.php?ctl=createLoc">Añadir</a></li>
		                <li id="readLoc" class="submenu--entrada"><a href="index.php?ctl=readLoc">Mostrar</a></li>
		            </ul>
		        </li>
		        <li class="menu--barra-principal--entrada">
		            <a href="#">Usuarios</a>
		            <ul class="submenu">
		                <li id="createU" class="submenu--entrada"><a href="index.php?ctl=createU">Añadir</a></li>
		                <li id="readU" class="submenu--entrada"><a href="index.php?ctl=readU">Mostrar</a></li>
		            </ul>
		        </li>
		    </ul>
		<?php } ?>
		</nav>
	</div>
	<div id="contenedor">
		<div class="colOpciones">
			<?php
			if (isset($_SESSION['colInfo'])) {
				echo $_SESSION['colInfo'];
			}
			?>			
		</div>
		<div class="contenido">
			<?php echo $contenido ?>
		</div>
	</div>
	<footer>
		<div>
			<?php
				echo "Autor: ".Config::$autor."<br>";
				echo "Proyecto: ".Config::$titulo."<br>";
			?>
			<a class="aPie" href="../app/docs/doc.pdf" target="_blank">Documentación</a>
		</div>
		<div>
			<?php
				echo "Fecha: ".Config::$fecha."<br>";
				echo Config::$empresa." | ".Config::$curso."<br>";
				echo "Tema: ".Config::$tema;
			?>
		</div>
		<div>
			<a class="aPie" href="../app/docs/DWST5PROYECTO5.pdf" target="_blank">Enunciado PROY.5</a><br>
			<a class="aPie" href="../app/docs/DWST6PROYECTO6.pdf" target="_blank">Enunciado PROY.6</a>
		</div>
		<div>
			<a class="aPie" href="../app/docs/DWST7PROYECTO7.pdf" target="_blank">Enunciado PROY.7</a><br>
			<a class="aPie" href="../app/docs/DWST8PROYECTO8.pdf" target="_blank">Enunciado PROY.8</a>
		</div>
	</footer>
</body>
</html>
