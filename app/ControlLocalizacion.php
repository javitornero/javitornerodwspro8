<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
	require_once __DIR__ . '/utils/utils_control.php';
    require_once __DIR__ . "/Localizacion.php";
/**
* 
*/
class ControlLocalizacion {

	public function createLoc() {
		$modelo=obtenerModelo();
		$id=$modelo->newId("localizaciones");
        $params = array('id' => $id,'nom' => '',);
		if (isset($_REQUEST['sbCreateLoc'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    $nom = recoge('nom'); // comprobar campos formulario
            if ($id!="" && $nom!="") {
				$localizacion = new Localizacion($id, $nom, '0');
                if ($modelo->createLocalizacion($localizacion)) {
                    header('Location: index.php?ctl=readLoc');
                } else {
                    $params = array('id' => $_REQUEST['id'],'nom' => $_REQUEST['nom'],);
                    $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                }
            } else {
                $params = array('id' => $_REQUEST['id'],'nom' => $_REQUEST['nom'],);
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
        }
		require __DIR__ . '/templates/localizacionCreate.php';
	}
	
	public function readLoc() {
		$modelo=obtenerModelo();
        $params = array('localizaciones' => $modelo->readLocalizaciones(),);
        require __DIR__ . '/templates/localizacionRead.php';
	}

	public function updateLoc() {
		$modelo=obtenerModelo();
        $params = array('id' => '', 'nom' => '','localizaciones' => $modelo->readLocalizaciones(),);
		if (isset($_REQUEST['id'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    $nom = recoge('nom'); // comprobar campos formulario
		    if ($id!="" && $nom!="") {
                if (isset($_REQUEST['sbUpdateLoc'])) {
                    $localizacion = new Localizacion($id, $nom, "");
                    if ($modelo->updateLocalizacion($localizacion)) {
                        header('Location: index.php?ctl=readLoc');
                    } else {
                        $params = array('id' => $id,'nom' => $nom,'localizaciones' => $modelo->readLocalizaciones(),);
                        $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                    }
                }else{
                    $params = array('id' => $id,'nom' => $nom,'localizaciones' => $modelo->readLocalizaciones(),);
                    $params['mensaje'] = 'Proceso de actualización de registro.';
                }
            } else {
                $params = array('id' => $id,'nom' => $nom,'localizaciones' => $modelo->readLocalizaciones(),);
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
        }
		require __DIR__ . '/templates/localizacionUpdate.php';
	}

	public function delLoc() {
		$modelo=obtenerModelo();
        $params = array('id' => '', 'nom' => '','localizaciones' => $modelo->readLocalizaciones(),);
		if (isset($_REQUEST['id'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    if ($id!="") {
				$localizacion = new Localizacion($id, "", "");
				if ($modelo->deleteLocalizacion($localizacion)) {
                    header('Location: index.php?ctl=readLoc');
                } else {
                    $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                }
            } else {
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
		}
		require __DIR__ . '/templates/localizacionDelete.php';
	}
}

?>