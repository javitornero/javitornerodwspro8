<?php
/**
* 
*/
    class Config{
		static public $titulo =  "Gestión de usuarios";
		static public $autor =  "Javier Tornero Montellano";
		static public $fecha = "21/01/2017";
		static public $empresa =  "CEEDCV";
		static public $curso =  "2016-17";
		static public $tema =  "T8.MVC";
		static public $bdhostname =  "localhost";
		static public $bdnombre =  "dws_bd";
		static public $bdusuario =  "alumno";
		static public $bdclave =  "alumno";
		
		static public $consultaCreaBD = "CREATE DATABASE IF NOT EXISTS dws_bd DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_general_ci';";
		static public $consultaCreaTablas = "CREATE TABLE IF NOT EXISTS `localizaciones` (
			`id` int(11) NOT NULL,
			`nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

			CREATE TABLE IF NOT EXISTS `usuarios` (
			`id` int(11) NOT NULL,
			`nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
			`id_loc` int(11) NOT NULL,
			PRIMARY KEY (`id`),
			KEY `id_loc` (`id_loc`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

			CREATE TABLE IF NOT EXISTS `administradores` (
			`id` int(11) NOT NULL,
			`nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
			`password` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
			`tipo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
			PRIMARY KEY (`id`),  UNIQUE KEY `nom` (`nom`)

			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

			INSERT INTO `administradores` (`id`, `nom`, `password`, `tipo`) VALUES (1, 'admin', 'admin', 'Administrador');

			
			ALTER TABLE `usuarios` ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_loc`) REFERENCES `localizaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;";

		static public $consultaCreaU = "CREATE USER 'alumno'@'localhost' IDENTIFIED BY 'alumno'";
		static public $consultaPrivilegios = 
				"GRANT ALL ON 'ceedcv'.* TO 'alumno'@'localhost';
                FLUSH PRIVILEGES;";
    }

?>