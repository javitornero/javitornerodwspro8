<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
	require_once __DIR__ . '/utils/utils_control.php';
    require_once __DIR__ . "/Localizacion.php";
/**
* 
*/
class ControlUsuario {

	public function createU() {
		$modelo=obtenerModelo();
		$id=$modelo->newId("usuarios");
        $params = array('id' => $id,'nom' => '','localizaciones' => $modelo->readLocalizaciones(),);
		if (isset($_REQUEST['sbCreateU'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    $nom = recoge('nom'); // comprobar campos formulario
		    $id_loc = recoge('id_loc'); // comprobar campos formulario
            if ($id!="" && $nom!="" && $id_loc!="") {
                $localizacion = new Localizacion($id_loc, "", "");
                $usuario = new Usuario($id, $nom, $localizacion);
                if ($modelo->createUsuario($usuario)) {
                    header('Location: index.php?ctl=readU');
                } else {
                    $params = array('id' => $id,'nom' => $nom,'id_loc' => $id_loc, 'localizaciones' => $modelo->readLocalizaciones(),);
                    $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                }
            } else {
                $params = array('id' => $id,'nom' => $nom,'id_loc' => $id_loc, 'localizaciones' => $modelo->readLocalizaciones(),);
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
        }
		require __DIR__ . '/templates/usuarioCreate.php';
	}
	
	public function readU() {
		$modelo=obtenerModelo();
        $params = array('usuarios' => $modelo->readUsuarios(),);
        require __DIR__ . '/templates/usuarioRead.php';
	}

	public function updateU() {
		$modelo=obtenerModelo();
        $params = array('id' => '', 'nom' => '', 'id_loc' => '', 'usuarios' => $modelo->readUsuarios(),'localizaciones' => $modelo->readLocalizaciones(),);
		if (isset($_REQUEST['id'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    $nom = recoge('nom'); // comprobar campos formulario
		    $id_loc = recoge('id_loc'); // comprobar campos formulario
		    if ($id!="" && $nom!="" && $id_loc!="") {
                if (isset($_REQUEST['sbUpdateU'])) {
                    $localizacion = new Localizacion($id_loc, "", "");
                    $usuario = new Usuario($id, $nom, $localizacion);
                    if ($modelo->updateUsuario($usuario)) {
                        header('Location: index.php?ctl=readU');
                    } else {
                        $params = array('id' => $id,'nom' => $nom,'id_loc' => $id_loc,'usuarios' => $modelo->readUsuarios(),'localizaciones' => $modelo->readLocalizaciones(),);
                        $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                    }
                }else{
                    $params = array('id' => $id,'nom' => $nom,'id_loc' => $id_loc,'usuarios' => $modelo->readUsuarios(), 'localizaciones' => $modelo->readLocalizaciones(),);
                    $params['mensaje'] = 'Proceso de actualización de registro.';
                }
            } else {
                $params = array('id' => $id,'nom' => $nom,'id_loc' => $id_loc,'usuarios' => $modelo->readUsuarios(),'localizaciones' => $modelo->readLocalizaciones(),);
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
        }
		require __DIR__ . '/templates/usuarioUpdate.php';
	}

	public function delU() {
		$modelo=obtenerModelo();
        $params = array('id' => '','usuarios' => $modelo->readUsuarios(),);
		if (isset($_REQUEST['id'])) {
		    $id = recoge('id'); // comprobar campos formulario
		    if ($id!="") {
				$usuario = new Usuario($id, "", "");
				if ($modelo->deleteUsuario($usuario)) {
                    header('Location: index.php?ctl=readU');
                } else {
                    $params['mensaje'] = 'Operación no realizada, ha habido algún error.';
                }
            } else {
                $params['mensaje'] = 'Operación no realizada, se han encontrado campos vacíos.';
            }
		}
		require __DIR__ . '/templates/usuarioDelete.php';
	}
}

?>