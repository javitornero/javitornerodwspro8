<?php

// Función que limpia la entrada de datos en formularios
function recoge($campo) { 
	if (isset($_REQUEST[$campo])) {
		$valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
	} else {
		$valor = "";
	};
	return $valor;
}

function obtenerModelo() {
	if (isset($_SESSION["modelo"])) {
		$tipo = $_SESSION["modelo"];
		switch ($tipo) {
			case 'fichero':
				include_once("/../ModeloFichero.php");
				$modelo = new ModeloFichero();
				break;
			case 'mysql':
				include_once('/../ModeloMysql.php');
				$modelo = new ModeloMysql(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
				break;
		}
		return $modelo;
	}else{
		echo "Error : No hay modelo";
	}
}

function inicializaModelo(){
	if (!isset($_SESSION["modelo"])) {
		$_SESSION["modelo"]="mysql";
	}
}

function cambiaModelo(){
	if (isset($_REQUEST['sbModelo'])) {
		if ($_REQUEST['selectModelo']=="fichero" || $_REQUEST['selectModelo']=="mysql") {
			$_SESSION["modelo"]=$_REQUEST['selectModelo'];
		}
	}
}

?>